FROM golang:1.12.0-alpine3.9 as builder
RUN mkdir /app
ADD hi.go /app
WORKDIR /app
RUN go build -o hi .

FROM alpine:latest as host
RUN apk add --no-cache ca-certificates
COPY --from=builder /app/hi /
WORKDIR /
EXPOSE 8090
CMD ["/hi"]

