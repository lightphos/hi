# hi

## Set docker registry access in k8s

cat ~/.docker/certs.d/gitlab.lightphos.com\:5555/ca.crt | minikube ssh "sudo mkdir -p /etc/docker/certs.d/gitlab.lightphos.com:5555 && sudo tee /etc/docker/certs.d/gitlab.lightphos.com:5555/ca.crt" > /dev/null

## Manual k8s deployment

kubectl apply -f manifest/deployment.yaml
kubectl expose deployment hi-deployment --type=NodePort --port=8090

## Namespace
kubectl create -f manifest/namespace-dev.json 
get namespaces --show-labels | grep name=

or simply

kubectl create ns lightphos

kubectl apply -f manifest/rbac.yml
kubectl get -n lightphos secret $(kubectl get -n lightphos secret | grep gitlab-ci | cut -f 1 -d ' ') -o yaml

## Secret for docker access
kubectl create secret docker-registry gitlab.lightphos --docker-server=https://gitlab.lightphos.com:5555 --docker-username=USER --docker-password=PWD --namespace=lightphos
