package main

import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hi [%s]\n", r.URL.Path[1:])
	})
	http.ListenAndServe(":8090", nil)
}
